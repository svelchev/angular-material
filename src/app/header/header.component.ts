import { Component, Input, ChangeDetectorRef, ChangeDetectionStrategy, NgZone } from '@angular/core';
import { SettingsComponent } from './settings.component';
import { EditorComponent } from '../editor/editor.component';
import { CollectorComponent } from '../collector/collector.component';
import { AppComponent } from '../app.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'tripetto-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
  @Input() app: AppComponent;
  @Input() editor: EditorComponent;
  @Input() collector: CollectorComponent;
  isSettingsOpened = false;

  constructor(private changeDetector: ChangeDetectorRef, private dialog: MatDialog, private ngZone: NgZone) {}

  /** Opens the settings dialog. */
  openSettings(): void {
    this.ngZone.run(() => {
      this.isSettingsOpened = true;

      this.dialog
        .open(SettingsComponent, {
          width: '90%',
          maxWidth: '500px',
          data: {
            collector: this.collector
          }
        })
        .afterClosed()
        .subscribe(() => {
          this.isSettingsOpened = false;

          this.changed();
        });
    });
  }

  /**
   * We need this function since this component is a sibling of the collector component.
   * When the collector component detects a change, its siblings are not changed.
   */
  changed() {
    this.changeDetector.detectChanges();
  }
}
