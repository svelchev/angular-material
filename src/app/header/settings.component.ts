import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CollectorComponent } from '../collector/collector.component';

@Component({
  selector: 'tripetto-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: {
      collector: CollectorComponent;
    }
  ) {}
}
