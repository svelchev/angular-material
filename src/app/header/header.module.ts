import { HeaderComponent } from './header.component';
import { SettingsComponent } from './settings.component';
import { CollectorModule } from '../collector/collector.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatDialogModule,
  MatIconModule,
  MatToolbarModule,
  MatCardModule,
  MatSlideToggleModule,
  MatRadioModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    CollectorModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatSlideToggleModule,
    MatRadioModule
  ],
  declarations: [HeaderComponent, SettingsComponent],
  entryComponents: [SettingsComponent],
  exports: [HeaderComponent]
})
export class HeaderModule {}
