import * as Tripetto from 'tripetto-collector';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Dropdown } from 'tripetto-block-dropdown/collector';
import { Component } from '@angular/core';

@Component({
  templateUrl: './dropdown.html'
})
export class DropdownBlockComponent extends BlockComponentFactory<DropdownBlock> {}

@Tripetto.block({
  type: 'node',
  identifier: 'tripetto-block-dropdown',
  ref: DropdownBlockComponent
})
export class DropdownBlock extends Dropdown {}
