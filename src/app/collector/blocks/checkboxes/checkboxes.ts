import * as Tripetto from 'tripetto-collector';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Checkboxes } from 'tripetto-block-checkboxes/collector';
import { Component } from '@angular/core';

@Component({
  templateUrl: './checkboxes.html',
  styleUrls: ['./checkboxes.scss']
})
export class CheckboxesBlockComponent extends BlockComponentFactory<CheckboxesBlock> {}

@Tripetto.block({
  type: 'node',
  identifier: 'tripetto-block-checkboxes',
  ref: CheckboxesBlockComponent
})
export class CheckboxesBlock extends Checkboxes {}
